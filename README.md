# Una vez clonado el proyecto los pasos siguientes serían

1. docker-compose up -d
2. docker exec -it ksqldb-cli ksql http://ksqldb-server:8088 (Estamos dentro del servidor de ksql)

Ejecutar los comando del ksql anajo

# KSQL

1. CREATE STREAM finance (symbol VARCHAR, price DOUBLE, volume DOUBLE, timestamp VARCHAR)
   WITH (kafka_topic='kafka-final', value_format='json', partitions=1);

#########################################
############ EXERCISES ##################
#########################################

2. CREATE TABLE promedio_ponderado AS
   SELECT symbol,
   SUM(price * volume) AS precio_volumen,
   SUM(volume) as volumen,
   SUM(price * volume) / SUM(volume) AS promedio_ponderado
   FROM finance
   GROUP BY symbol
   EMIT CHANGES;

CREATE TABLE transacciones_simbolo AS
SELECT symbol,
count(*) AS cantidad_transaccion,
price
FROM finance
GROUP BY symbol
EMIT CHANGES;

CREATE TABLE maximo_precio AS
SELECT symbol,
MAX(price) AS maximo_precio
FROM finance
GROUP BY symbol
EMIT CHANGES;

CREATE TABLE minimo_precio AS
SELECT symbol,
MAX(price) AS minimo_precio
FROM finance
GROUP BY symbol
EMIT CHANGES;

# Ahora configurar en python el producer

# Ejecutar los comandos de python abajo

# PYTHON

1. cd monitoring
2. python3 -m venv env
3. source env/bin/activate
4. pip install --upgrade pip
5. pip install kafka-python
6. pip install websocket-client
7. python monitoring/producer.py

# Para generar el API key de Finnhub registrarse en la pagina y luego acceder a este link

- https://finnhub.io/dashboard
